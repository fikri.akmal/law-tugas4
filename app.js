const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.SERVER_PORT || 3000

const app = express();
app.use(bodyParser.json())


let database = {
    1906307132: "Fikri Akmal"
}

app.post("/update", (req, res) => {
    const nama = req.body.nama
    const npm = req.body.npm
    database[npm] = nama

    return res.status(200).json({
        status: "OK"
    })
})

app.get('/read/:npm/:cache_key', (req, res) => {
    console.log("hit")
    const npm = req.params.npm
    if (database[npm] !== undefined) {
        const response = {
            status: "OK",
            nama: database[npm],
            npm: npm
        }
        return res.status(200).json(response)
    } else {
        return res.status(404).json({
            status: 'Not Found'
        })
    }
})

app.get('/read/:npm', (req, res) => {
    const npm = req.params.npm
    if (database[npm] !== undefined) {
        const response = {
            status: "OK",
            nama: database[npm],
            npm: npm
        }
        return res.status(200).json(response)
    } else {
        return res.status(404).json({
            status: 'Not Found'
        })
    }
})


app.listen(port, () => {
    console.log(`App started on PORT ${port}`)
})